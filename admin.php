<?php

  require_once("includes/initializer.php");
  require_once("assets/swift/swift_required.php");
  require_once("assets/phpexcel/PHPExcel.php");



  if(!$session->is_logged_in()) {

    redirect_to("".base_url()."/");

  } else {

    if($_SESSION['user_type'] != "admin"  && $_SESSION['user_type'] != "finance") {

      redirect_to("".base_url()."/");

    }

  }

 

  if(isset( $_POST['articlecategory'])) {



    if(!$session->is_logged_in()) {



      redirect_to("".base_url()."/login");

    }



    $article = new ArticleCategory();



    

    $article->art_cat_name = tostandardize(trim($_POST['art_cat_name']));

    

    if($article->save()) {



      $session->message("Article Category Added Successfully");

            redirect_to("".base_url()."/administrator");



        

    } else {



      $session->message("Article Category Failed to Add");

            redirect_to("".base_url()."/administrator");



    } 



  } else if(isset( $_POST['postcategory'])) {



    if(!$session->is_logged_in()) {



      redirect_to("".base_url()."/login");

    }



    $post = new PostCategory();



    

    $post->post_cat_name = tostandardize(trim($_POST['post_cat_name']));

    

    if($post->save()) {



      $session->message("Post Category Added Successfully");

            redirect_to("".base_url()."/administrator");



        

    } else {



      $session->message("Post Category Failed to Add");

            redirect_to("".base_url()."/administrator");



    } 



  } else if(isset( $_POST['resourcecategory'])) {



    if(!$session->is_logged_in()) {



      redirect_to("".base_url()."/login");

    }



    $resource = new ResourceCategory();



    

    $resource->res_cat_name = tostandardize(trim($_POST['res_cat_name']));

    

    if($resource->save()) {



      $session->message("Resource Category Added Successfully");

            redirect_to("".base_url()."/administrator");



        

    } else {



      $session->message("Resource Category Failed to Add");

            redirect_to("".base_url()."/administrator");



    } 



  }  else if(isset( $_POST['tomaincategory'])) {



    if(!$session->is_logged_in()) {



      redirect_to("".base_url()."/login");

    }



    $tomain = new ToMainCategory();



    

    $tomain->main_cat_name = tostandardize(trim($_POST['main_cat_name']));

    

    if($tomain->save()) {



      $session->message("Test Ocean Main Category Added Successfully");

            redirect_to("".base_url()."/administrator");



        

    } else {



      $session->message("Test Ocean Main Category Failed to Add");

            redirect_to("".base_url()."/administrator");



    } 



  } else if(isset( $_POST['tosubcategory'])) {



    if(!$session->is_logged_in()) {



      redirect_to("".base_url()."/login");

    }



    $tosub = new ToSubCategory();



    $tosub->main_cat_id = trim($_POST['main_cat_id']);

    $tosub->sub_cat_name = tostandardize(trim($_POST['sub_cat_name']));

    $tosub->sub_cat_description = trim($_POST['sub_cat_description']);

    

    if($tosub->save()) {



      $session->message("Test Ocean Sub Category Added Successfully");

            redirect_to("".base_url()."/administrator");



        

    } else {



      $session->message("Test Ocean Sub Category Failed to Add");

            redirect_to("".base_url()."/administrator");



    } 



  } else if(isset($_POST['usefullink'])) {

      if(!$session->is_logged_in()) {

        redirect_to("".base_url()."/login");

      }

      $link = new UsefulLink();

      $link->link_title = trim($_POST['link_title']);
      $link->link_url = trim($_POST['link_url']);

      
      if($link->save()) {

        $session->message("Useful Link Added Successfully");

        redirect_to("".base_url()."/administrator");
        

      } else {

        $session->message("Useful link Failed to Add");

        redirect_to("".base_url()."/administrator");

      } 

  } else if(isset($_POST['notify'])) {

      if(!$session->is_logged_in()) {

        redirect_to("".base_url()."/login");

      }

        $notify = new Notification();

        $notify->notification_title = trim($_POST['notification_title']);
        $notify->notification_to = trim($_POST['notification_to']);
        $notify->notification_description = trim($_POST['notification_description']);
        $time = time();
        $notify->notification_time = date('Y-m-d H:i:s', $time);


        
        if($notify->save()) {

        $transport = Swift_SmtpTransport::newInstance('smtpout.asia.secureserver.net', 3535)
        ->setUsername('test@stimulatingneurons.in')
        ->setPassword('v2ea8H3xlA')
        ;


        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        $result = User::get_email($notify->notification_to);

        // Create a message
        $message = Swift_Message::newInstance('Notification');
        $message->setFrom(array('test@stimulatingneurons.in' => 'CJC Forum'));

        while($user = mysqli_fetch_assoc($result)) {
    
          $mid = $user['user_email'];
          $message->addTo($mid);
          
        }
        
        $message->setBody('Hi,

Greetings from CJC Forum!

'.$notify->notification_title.'
          
'.$notify->notification_description.'

                ', 'text/html');

        // Send the message
        $numSent = $mailer->send($message); 

        if($numSent >= 1) {
          $session->message("Notification Sent to all ".$numSent." users.");
        } else {
          $session->message("Notification was not sent by mail to users.");
        }

        redirect_to("".base_url()."/administrator");
        

      } else {

        $session->message("Notification Failed to send");

        redirect_to("".base_url()."/administrator");

      } 

  } else if(isset($_POST['event'])) {

      if(!$session->is_logged_in()) {

        redirect_to("".base_url()."/login");

      }

      $event = new Event();

      $event->event_title = trim($_POST['event_title']);
      $event->event_description = trim($_POST['event_description']);      
      $event->event_url = trim($_POST['event_url']);
      $time = time();
      $event->event_time = date('Y-m-d H:i:s', $time);


      
      if($event->save()) {

        $session->message("event Sent Successfully");

        redirect_to("".base_url()."/administrator");
        

      } else {

        $session->message("event Failed to send");

        redirect_to("".base_url()."/administrator");

      } 

  } else if(isset($_POST['test'])) {

      if(!$session->is_logged_in()) {

        redirect_to("".base_url()."/login");

      }

      $test = new Test();

      $test->course_id = trim($_POST['test_course']);
      $test->test_title = trim($_POST['test_title']);
      $test->test_description = trim($_POST['test_description']);
      $test->test_category = strtoupper(trim($_POST['test_category']));   
      $test->test_total = trim($_POST['test_total']);       
      $test->test_on = trim($_POST['test_on']);
          
      if($test->save()) {

        $session->message("Test Created Successfully");

        redirect_to("".base_url()."/administrator");
        

      } else {

        $session->message("Test Failed to Create");

        redirect_to("".base_url()."/administrator");

      } 

  } else if(isset($_POST['testdata'])) {

    if($_FILES['file']['tmp_name']){
      if(!$_FILES['file']['error'])
      {
          $inputFile = $_FILES['file']['tmp_name'];
          $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
          //if($extension == 'XLSX' || $extension == 'ODS'){

              //Read spreadsheeet workbook
              try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFile);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
              } catch(Exception $e) {
                     
                    $session->message($e->getMessage());
                    redirect_to("".base_url()."/administrator");
              }
             
              $sheet = $objPHPExcel->getActiveSheet();
              $highestRow = $sheet->getHighestRow(); 
              $highestColumn = $sheet->getHighestColumn();
              $error = array();

              $testId = $_POST['testid'];

              $error ="[ ";
              $flag=0;

              for ($row = 2; $row <= $highestRow; $row++){ 
                //  Read a row of data into an array

                if($sheet->getCellByColumnAndRow(0, $row)->getValue() != "") { 
                  $student = Student::find_by_id($sheet->getCellByColumnAndRow(0, $row)->getValue());



                  if(!$student) {
                    
                    $flag=1;

                  }

                  $testscore = TestScore::find_by_test($sheet->getCellByColumnAndRow(0, $row)->getValue(),$testId);

                  if(!$testscore) {
                    $testscore = new TestScore();
                  }
                  


                  $testscore->test_id = $testId;
                  $testscore->student_id = $sheet->getCellByColumnAndRow(0, $row)->getValue();
                  $testscore->student_name = $sheet->getCellByColumnAndRow(1, $row)->getValue();
                  $testscore->attempt = $sheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
                  $testscore->correct_ans = $sheet->getCellByColumnAndRow(3, $row)->getValue();
                  $testscore->wrong_ans = $sheet->getCellByColumnAndRow(4, $row)->getValue();
                  $testscore->accuracy = $sheet->getCellByColumnAndRow(5, $row)->getCalculatedValue();
                  $testscore->total_marks = $sheet->getCellByColumnAndRow(6, $row)->getCalculatedValue();
                  $testscore->remarks = $sheet->getCellByColumnAndRow(7, $row)->getValue();

                  if($testscore->save()) {
                    
                  } else {
                    $error .= $testscore->student_id.", ";
                  }
                }
          
              }
              if($flag==1){
                $error.="]";
                $session->message("Error in roll nos<br />".$error);
              }else {
                $session->message("Score Upload Success");
              }              

              redirect_to("".base_url()."/administrator");

      }
      else{
        $session->message("Spreadsheet Error");

        redirect_to("".base_url()."/administrator");
      }
    }
  }

elseif(isset( $_POST['assignmentinfo'])) {
  if(!$session->is_logged_in()) {

        redirect_to("".base_url()."/login");

      }

  $assignment = new Assignment();

  
  $assignment->course_id = trim($_POST['sCourse']);
  $assignment->assignment_topic = trim($_POST['aTopic']);
  $assignment->assignment_no = trim($_POST['aNo']);
  $assignment->assignment_subject = trim($_POST['aSubject']);
  $assignment->subject_id = trim($_POST['sId']);
  $assignment->assign_date = trim($_POST['assignDate']);
  $assignment->deadline_date = trim($_POST['deadlineDate']);

  


    if($assignment->save()) {


      $session->message("Assignment Details Added");        

      redirect_to("".base_url()."/administrator");

    } else {


      $session->message("Could not add assignment info. try again");

      redirect_to("".base_url()."/administrator");

    }
  }

else if(isset($_POST['assignmentdata'])) {

    if($_FILES['file']['tmp_name']){
      if(!$_FILES['file']['error'])
      {
          $inputFile = $_FILES['file']['tmp_name'];
          $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
          //if($extension == 'XLSX' || $extension == 'ODS'){

              //Read spreadsheeet workbook
              try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFile);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
              } catch(Exception $e) {
                     
                    $session->message($e->getMessage());
                    redirect_to("".base_url()."/administrator");
              }
             
              $sheet = $objPHPExcel->getActiveSheet();
              $highestRow = $sheet->getHighestRow(); 
              $highestColumn = $sheet->getHighestColumn();
              $error = array();

              $assignmentID = $_POST['assignmentid'];

              $error ="[ ";
              $flag=0;

              for ($row = 2; $row <= $highestRow; $row++){ 
                //  Read a row of data into an array

                if($sheet->getCellByColumnAndRow(0, $row)->getValue() != "") { 

                  $student = Student::find_by_id($sheet->getCellByColumnAndRow(0, $row)->getValue());



                  if(!$student) {
                    
                    $flag=1;

                  }

                  $studentassignment = StudentAssignment::find_by_assignment($sheet->getCellByColumnAndRow(0, $row)->getValue(),$assignmentID);
                  $dummy = "";

                  if(!$studentassignment) {
                   $studentassignment = new StudentAssignment();
                  } /*else {
                    $dummy = "found";
                  }*/
                  
                  $studentassignment->assignment_id = $assignmentID;
                  $studentassignment->student_id = $sheet->getCellByColumnAndRow(0, $row)->getValue();
                  $studentassignment->student_name = $sheet->getCellByColumnAndRow(1, $row)->getValue();
                  $studentassignment->assignment_subject = $sheet->getCellByColumnAndRow(2, $row)->getValue();
                  $studentassignment->assignment_topic = $sheet->getCellByColumnAndRow(3, $row)->getValue();
                  $studentassignment->assignment_no = $sheet->getCellByColumnAndRow(4, $row)->getValue();
                  $studentassignment->assigned_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCellByColumnAndRow(5, $row)->getValue()));
                  $studentassignment->deadline_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCellByColumnAndRow(6, $row)->getValue()));
                  $studentassignment->submitted_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCellByColumnAndRow(7, $row)->getValue()));
                  $studentassignment->assignment_submitted = $sheet->getCellByColumnAndRow(8, $row)->getValue();

                  if($studentassignment->save()) {
                    
                  } else {
                    
                    $error .= $studentassignment->student_id." ". $database->get_error() .", ";
                  }
                }
          
              }
              if($flag==1){
                $error.="]";
                $session->message("Error in roll nos<br />".$error);
              }else {
                $session->message(" Upload Success".$dummy);
              }              

              redirect_to("".base_url()."/administrator");

      }
      else{
        $session->message("Spreadsheet Error");

        redirect_to("".base_url()."/administrator");
      }
    }
  }



   else if(isset($_POST['attendance'])) {

    if($_FILES['file']['tmp_name']){
      if(!$_FILES['file']['error'])
      {
          $inputFile = $_FILES['file']['tmp_name'];
          $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
          //if($extension == 'XLSX' || $extension == 'ODS'){

              //Read spreadsheeet workbook
              try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFile);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
              } catch(Exception $e) {
                     
                    $session->message($e->getMessage());
                    redirect_to("".base_url()."/administrator");
              }
             
              $sheet = $objPHPExcel->getActiveSheet();
              $highestRow = $sheet->getHighestRow(); 
              $highestColumn = $sheet->getHighestColumn();
              $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
              $error = array();

              $course = trim($_POST['att_course']);

              $ses = "";

              // $session->message(" row : ".$highestRow. " col : ".$highestColumnIndex);

              // redirect_to("".base_url()."/administrator");


              for ($row = 2; $row <= $highestRow; $row++){

                if($sheet->getCellByColumnAndRow(0, $row)->getValue() != "") { 

                  $student = Student::find_by_id($sheet->getCellByColumnAndRow(0, $row)->getValue());

                  if($student == false) {
                    $student = new Student();
                    $student->student_name = $sheet->getCellByColumnAndRow(1, $row)->getValue();
                    $student->course_id = $course;
                    $student->targeted_course = courseStandard($sheet->getCellByColumnAndRow(2, $row)->getValue());
                    if(!empty($student->student_name)){
                      $student->save();
                    } else {
                      $ses .= "Student Name error in row ". $row."<br>";
                    }
                  }     

                  $stu = StudentAttendance::find_by_id($sheet->getCellByColumnAndRow(0, $row)->getValue(), $sheet->getCellByColumnAndRow(4, $row)->getValue(), $sheet->getCellByColumnAndRow(3, $row)->getValue());                   

                  if($stu == false) {
                    $stu = new StudentAttendance();
                    $stu->student_id = $sheet->getCellByColumnAndRow(0, $row)->getValue();
                    $stu->course_id =  $course;
                  }                  

                  
                  $stu->att_year = $sheet->getCellByColumnAndRow(3, $row)->getValue();
                  $stu->att_month = $sheet->getCellByColumnAndRow(4, $row)->getValue();
                  $stu->att_term = $sheet->getCellByColumnAndRow(5, $row)->getValue();

                  for ($column = 6; $column <= $highestColumnIndex; $column++) { 
                    $key = $sheet->getCellByColumnAndRow($column, 1)->getValue();
                    $val = $sheet->getCellByColumnAndRow($column, $row)->getValue();

                    //$data .= "Key ". $key ." Val ". $val. "\n";

                    if($val == ""){
                      $val = "A";
                    }

                    $val = strtoupper($val);
                    switch($key) {
                      
                      case 1: $stu->d1 = $val; break;
                      case 2: $stu->d2 = $val; break;
                      case 3: $stu->d3 = $val; break;
                      case 4: $stu->d4 = $val; break;
                      case 5: $stu->d5 = $val; break;
                      case 6: $stu->d6 = $val; break;
                      case 7: $stu->d7 = $val; break;
                      case 8: $stu->d8 = $val; break;
                      case 9: $stu->d9 = $val; break;
                      case 10: $stu->d10 = $val; break;
                      case 11: $stu->d11 = $val; break;
                      case 12: $stu->d12 = $val; break;
                      case 13: $stu->d13 = $val; break;
                      case 14: $stu->d14 = $val; break;
                      case 15: $stu->d15 = $val; break;
                      case 16: $stu->d16 = $val; break;
                      case 17: $stu->d17 = $val; break;
                      case 18: $stu->d18 = $val; break;
                      case 19: $stu->d19 = $val; break;
                      case 20: $stu->d20 = $val; break;
                      case 21: $stu->d21 = $val; break;
                      case 22: $stu->d22 = $val; break;
                      case 23: $stu->d23 = $val; break;
                      case 24: $stu->d24 = $val; break;
                      case 25: $stu->d25 = $val; break;
                      case 26: $stu->d26 = $val; break;
                      case 27: $stu->d27 = $val; break;
                      case 28: $stu->d28 = $val; break;
                      case 29: $stu->d29 = $val; break;
                      case 30: $stu->d30 = $val; break;
                      case 31: $stu->d31 = $val; break; 
                    }
                  }


                  if($stu->save()) {                    
                    
                  } else {
                    $ses .= "Error in row ". $row."<br>";                   
                    
                  }
                }
          
              }
              
              if($ses != ""){
                $session->message("".$ses);
              } else {
                $session->message("Attendance Sheet Uploaded Successfully");
              }              

              redirect_to("".base_url()."/administrator");

      }
      else{
        $session->message("Spreadsheet Error");

        redirect_to("".base_url()."/administrator");
      }
    }
  } 




?>



<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="">

    <meta name="author" content="">

    <base href="<?php echo base_url(); ?>/" />

    <link rel="icon" href="images/icon.png">



    <title>CJC Forum | Admin</title>



    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.css" rel="stylesheet">

    <link href="css/reset.css" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



  </head>



  <body class="double-nav">


    <nav class="navbar-fixed-top">
      <div class="navbar navbar-inverse">

        <div class="container">

          <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

              <span class="sr-only">Toggle navigation</span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

            </button>

            <a class="navbar-brand" href="<?php echo base_url(); ?>/"><img alt="CJC Forum" src="images/logo.png"></a>

          </div>

          <div id="navbar" class="collapse navbar-collapse">

            <ul class="nav navbar-nav">

              <li><a href="<?php echo base_url(); ?>/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
               
              <li><a href="articles"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Articles</a></li>

              <li><a href="discussions"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Discussions</a></li>

              <li><a href="test-ocean"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Test Ocean</a></li>

              <li><a href="resources"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> Resources</a></li>




              <?php 

                if($session->is_logged_in()) {

                  if($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "finance") {



              ?>

              <li class="active"><a href="administrator"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Admin Panel</a></li>

              <?php

                  }

                }

              ?>

            </ul>

            <ul class="nav navbar-nav navbar-right">



              <li class="dropdown">

                <a class="btn-default dropdown-toggle" id="profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"><span class = "glyphicon glyphicon-user"></span> <small><?php if($session->is_logged_in()) { echo $_SESSION['user_name']; } ?></small></a>

                   

                <ul class="dropdown-menu" aria-labelledby="profile">
                 
                  <?php 

                    if($session->is_logged_in()) {

                        echo "<li><a href=\"profile\"><span class = \"glyphicon glyphicon-user\"></span> My Profile </a>  </li>";  
                        echo "<li><a href=\"logout\"><span class = \"glyphicon glyphicon-off\"></span> Logout </a>  </li>";

                        if($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "finance" ) {
                          echo "<li role='separator' class='divider'></li>"; 
                          echo "<li class='dropdown-header'>Courses</li>";
                          $courses = StudentCourse::find_all();
                          // if($_SESSION['course_id'] == 0)
                          //   echo "<li class='active'><a href='changecourse/0'>All</a></li>";  
                          // else 
                          //   echo "<li><a href='changecourse/0'>All</a></li>";  
                          foreach ($courses as $course) { 
                            if($_SESSION['course_id'] == $course->course_id)
                              echo "<li class='active'><a href='changecourse/".$course->course_id."'>".$course->course_name."</a></li>";   
                            else
                              echo "<li><a href='changecourse/".$course->course_id."'>".$course->course_name."</a></li>";   
                           
                          }

                        }


                          
                    } else {

                            echo "<li> <a href=\"login\"> <span class = \"glyphicon glyphicon-log-in\"></span> Login </a> </li>";

                            echo "<li> <a href=\"signup\"><span class = \"glyphicon glyphicon-edit\"></span> Register </a>  </li>";

                    }

                  ?>

                  

                </ul>

              </li>

              

            </ul>

          </div><!--/.nav-collapse -->             

        </div>      

      </div>
      <div class="navbar navbar-inverse navbar-bottom">
        <div class="container">
          <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

              <span class="sr-only">Toggle navigation</span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>



            </button>

          </div>

          <div id="navbar" class="collapse navbar-collapse">

            <ul class="nav text-center navbar-nav">

              <li><a href="add-user/"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Add User / Group</a></li>
              <li><a href="messages/"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Send Messages</a></li>
              <li><a href="manage/"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Manage</a></li>
              <li><a href="add-student/"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Add Student</a></a></li>
              <li class="dropdown"> 
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-road" aria-hidden="true"></span> Tracker</a>
                <ul class="dropdown-menu">
                  <li><a href="performancetracker/"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Performance Tracker</a></li>
                  <li><a href="attendancetracker/"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Attendance Tracker</a></li>
                  <li><a href="scheduletracker/"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Schedule Tracker</a></li> 
                 <li><a href="assignmenttracker/"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Assignment Tracker</a></li>
                  <li><a href="facultyfeedbacktracker/"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Faculty Feedback</a></li>
                  <li><a href="generateanalytics/"><i class="fa fa-area-chart" aria-hidden="true"></i> Generate Analytics</a></li>
                  <li><a href="trackfeedbackinfo/"><i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i> Program Feedback</a></li>
                 
                </ul>
              </li>  
              <?php if($_SESSION['user_type'] == "admin"){ ?>
              <li><a href="online-test/"><i class="fa fa-hourglass-half" aria-hidden="true"></i> Online Test</a></a></li>   
              <?php } ?>        
                <?php if($_SESSION['user_type'] == "finance"){ ?>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> Budget</a>
               <ul class="dropdown-menu">
                 
                  <li><a href="feetracker/"><i class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></i> Revenue Tracker</a></li>
                   <li><a href="expendituretracker/"><i class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></i> Expenditure Tracker</a></li>
                   
                </ul>
              </li>           
               <?php } ?>
            </ul>

          </div><!--/.nav-collapse -->   
          
        </div>
      </div>
      
    </nav>

    




    <?php if(!empty($message)) { ?>



      <div class="alert alert-success alert-dismissible" role="alert">



        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>



        <strong>

          <?php echo output_message($message); ?>

        </strong>



      </div>



    <?php } ?>









    <div class="container">



      <div class="row">



        <ol class="breadcrumb">

          <li><a href="home">CJC Forum</a></li>

          <li class="active">Admin</a></li>

        </ol>



        <div class="col-md-3">

          <div class="panel panel-default">

          

            <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> Post Category</div>

              

              <ul class="list-group">

                <?php                   

                  $result = PostCategory::find_all();



                  while($category = mysqli_fetch_assoc($result)) {



                ?>

                <li class="list-group-item">

                  <?php echo $category['post_cat_name']; ?>

                  <a href="category/discussions/delete/<?php echo $category['post_cat_id']; ?>"><span class="glyphicon glyphicon-trash pull-right"> </span></a>

                </li>

                <?php

                  }

                ?>



              </ul>

            <div class="panel-footer">

              <form method="post">

                <div class="input-group">

                  <input type="text" name="post_cat_name" class="form-control" placeholder="Category Name" required>

                  <span class="input-group-btn">

                    <button class="btn btn-default" type="submit" name="postcategory">Add</button>

                  </span>

                </div>

              </form>

            </div>

          </div>

        </div>



        <div class="col-md-3">

          <div class="panel panel-default">

          

            <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> Articles Category</div>

              

              <ul class="list-group">

                <?php 



                  

                  $result = ArticleCategory::find_all();



                  while($category = mysqli_fetch_assoc($result)) {



                ?>

                <li class="list-group-item">

                  <?php echo $category['art_cat_name']; ?>

                  <a href="category/articles/delete/<?php echo $category['art_cat_id']; ?>"><span class="glyphicon glyphicon-trash pull-right"> </span></a>

                </li>

                <?php

                  }

                ?>



              </ul>

            <div class="panel-footer">

              <form method="post">

                <div class="input-group">

                  <input type="text" name="art_cat_name" class="form-control" placeholder="Category Name" required>

                  <span class="input-group-btn">

                    <button class="btn btn-default" type="submit" name="articlecategory">Add</button>

                  </span>

                </div>

              </form>

            </div>

          </div>

        </div>



        <div class="col-md-3">

          <div class="panel panel-default">

          

            <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> Resource Category</div>

              

              <ul class="list-group">

                <?php 



                  

                  $result = ResourceCategory::find_all();



                  while($category = mysqli_fetch_assoc($result)) {



                ?>

                <li class="list-group-item">

                  <?php echo $category['res_cat_name']; ?>

                  <a href="category/resources/delete/<?php echo $category['res_cat_id']; ?>"><span class="glyphicon glyphicon-trash pull-right"> </span></a>

                </li>

                <?php

                  }

                ?>



              </ul>

            <div class="panel-footer">

              <form method="post">

                <div class="input-group">

                  <input type="text" name="res_cat_name" class="form-control" placeholder="Category Name" required>

                  <span class="input-group-btn">

                    <button class="btn btn-default" type="submit" name="resourcecategory">Add</button>

                  </span>

                </div>

              </form>

            </div>

          </div>

        </div>



        <div class="col-md-3">

          <div class="panel panel-default">

          

            <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> T.O Main Category</div>

              

              <ul class="list-group">

                <?php                   

                  $result = ToMainCategory::find_all();



                  while($category = mysqli_fetch_assoc($result)) {



                ?>

                <li class="list-group-item">

                  <?php echo $category['main_cat_name']; ?>

                  <a href="category/testocean/main/delete/<?php echo $category['main_cat_id']; ?>"><span class="glyphicon glyphicon-trash pull-right"> </span></a>

                </li>

                <?php

                  }

                ?>



              </ul>

            <div class="panel-footer">

              <form method="post">

                <div class="input-group">

                  <input type="text" name="main_cat_name" class="form-control" placeholder="Category Name" required>

                  <span class="input-group-btn">

                    <button class="btn btn-default" type="submit" name="tomaincategory">Add</button>

                  </span>

                </div>

              </form>

            </div>

          </div>

        </div>

      </div>

      <div class="row">



        <div class="page-header">

          <h4>Test Ocean Sub Category</h4>

        </div>





        <?php $main_result = ToMainCategory::find_all();



          while($main_category = mysqli_fetch_assoc($main_result)) {



        ?>

        

        <div class="col-md-4">

          <div class="panel panel-default">

          

            <div class="panel-heading">

              <span class="glyphicon glyphicon-th-list"></span> <?php echo $main_category['main_cat_name']; ?>

            </div>

           

              

              <ul class="list-group">

                <?php                   

                  $sub_result = ToSubCategory::find_all_main($main_category['main_cat_id']);



                  if(mysqli_num_rows($sub_result) > 0) {



                ?>

                <li class="list-group-item text-left">

                  <a href="testocean/question/add/<?php echo $main_category['main_cat_id']; ?>" ><span class="glyphicon glyphicon-plus"></span> Questions </a>

                  <a class="pull-right" href="testocean/comprehension/add/<?php echo $main_category['main_cat_id']; ?>" ><span class="glyphicon glyphicon-plus"></span> Comprehension </a>

                </li>



                <?php 



                  while($sub_category = mysqli_fetch_assoc($sub_result)) {



                ?>

                <li class="list-group-item">

                  <?php echo $sub_category['sub_cat_name']; ?>

                  <a href="category/testocean/sub/delete/<?php echo $sub_category['sub_cat_id']; ?>"><span class="glyphicon glyphicon-trash pull-right"> </span></a>

                </li>

                <?php

                  }

                }

                ?>



              </ul>

            <div class="panel-footer">

              <form method="post">

                

                  <input type="hidden" name="main_cat_id" class="form-control" value="<?php echo $main_category['main_cat_id']; ?>">

                  <input type="text" name="sub_cat_name" class="form-control" placeholder="Category Name" required>

     

                  <textarea class="form-control" name="sub_cat_description" placeholder="Category Description"></textarea>



                  <button class="btn btn-default" type="submit" name="tosubcategory">Add</button>



              </form>

            </div>

          </div>

        </div>



        <?php } ?>



      </div>
      <div class="row">

        <div class="page-header">
          <h4>Extras</h4>
        </div>

        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Usefull Links
            </div>
            <div class="panel-body">
              <form method="post" >
                <div class= "form-group col-md-12">
                  <label for="input">Link Title</label>
                  <input type="text" class="form-control" id="input" name="link_title" Placeholder="Title" required>
                </div>
                <div class= "form-group col-md-12">
                  <label for="input">Link URL</label>
                  <input type="text" class="form-control" id="input" name="link_url" Placeholder="URL" required>
                </div>
                <div class="form-group text-center">
                  <input type="submit" name="usefullink" value = "Add Link" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>

        
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Event
            </div>
            <div class="panel-body">
              <form method="post" >
                <div class= "form-group col-md-12">
                  <label for="input">Event Title</label>
                  <input type="text" class="form-control" id="input" name="event_title" Placeholder="Title" required>
                </div>
                <div class= "form-group col-md-12">
                  <label for="input">Event Description</label>
                  <textarea class="form-control" rows="6" id="input" name="event_description" Placeholder="Description" required> </textarea>
                </div>
                <div class= "form-group col-md-12">
                  <label for="input">Event URL</label>
                  <input type="text" class="form-control" id="input" name="event_url" Placeholder="URL">
                </div>
                <div class="form-group text-center">
                  <input type="submit" name="event" value = "Add Event" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>

        
      </div>

      <div class="row">

        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Notifications
            </div>
            <div class="panel-body">
              <form method="post" >
                <div class= "form-group col-md-12">
                  <label for="input">Notification Title</label>
                  <input type="text" class="form-control" id="input" name="notification_title" Placeholder="Title" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="input1">Notification To</label>
                  <select name="notification_to" id="input1" class="form-control">
                    <option disabled selected>Select User Group</option>
                    <option value="0">ALL</option>
                    <?php $result = UserGroup::find_all();

                      foreach ($result as $group) { ?>

                        <option value="<?php echo $group->group_id; ?>"><?php echo $group->group_name; ?></option>
                    <?php } ?>
                  </select>

                </div>
                <div class= "form-group col-md-12">
                  <label for="input">Notification Description</label>
                  <textarea class="form-control notify" rows="10" id="input" name="notification_description" Placeholder="Description" required> </textarea>
                </div>
                <div class="form-group text-center">
                  <input type="submit" name="notify" value = "Send Notification" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>
        
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Test
            </div>
            <div class="panel-body">
              <form method="post" >
                <div class= "form-group col-md-12">
                  <label for="input">Test Title</label>
                  <input type="text" class="form-control" id="input" name="test_title" Placeholder="Title" required>
                </div>
                <div class= "form-group col-md-12">
                  <label for="input">Test Conducted On</label>
                  <input type="date" class="form-control" id="input" name="test_on" Placeholder="Test Conducted On">
                </div>
                <div class= "form-group col-md-12">
                  <label for="input">Test Total Marks</label>
                  <input type="text" class="form-control" id="input" name="test_total" Placeholder="Test Total Marks">
                </div>
                <div class="form-group col-md-12">

                  <label>Select Course</label>

                  <select class="form-control" name="test_course" required>
                    <option value="" selected disabled>Select Course</option>
                    <?php $courses = StudentCourse::find_all(); ?>
                    <?php foreach ($courses as $course) { ?>
                      <option value="<?php echo $course->course_id; ?>"><?php echo $course->course_name; ?>(<?php echo $course->course_type; ?>)</option>
                    <?php } ?>
                    
                  </select>

                </div>
                <div class="form-group col-md-12">
                 <label for="input1">Test Category</label>
                <?php if($_SESSION['course_id'] == 14 || $_SESSION['course_id'] == 15){ ?>
               
                  
                  <select name="test_category" id="input1" class="form-control">
                    <option disabled selected>Select Category</option>
                    <option value="Economy">Economy</option>
                    <option value="History">History</option>
                    <option value="Polity">Polity</option> 
                       <option value="Geography">Geography</option> 
                    <option value="CurrentAffairs">Current Affairs</option> 
                             
                  </select>

                 <?php } else { ?>
              
               
                  <select name="test_category" id="input1" class="form-control">
                    <option disabled selected>Select Category</option>
                    <option value="PHYSICS">Physics</option>
                    <option value="CHEMISTRY">Chemistry</option>
                    <option value="MATHEMATICS">Mathematics</option> 
                    <option value="BIOLOGY">Biology</option>                   
                  </select>

                
                <?php } ?>
                 </div>
                <div class= "form-group col-md-12">
                  <label for="input">Test Description</label>
                  <textarea class="form-control" rows="6" id="input" name="test_description" Placeholder="Description" required> </textarea>
                </div>
                
                <div class="form-group text-center">
                  <input type="submit" name="test" value = "Add Test" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Test Scores
            </div>
            <div class="panel-body">
              <form method="post" enctype="multipart/form-data">
                <div class="form-group col-md-12">
                  <label for="input1">Test Name</label>
                  <select name="testid" id="input1" class="form-control">
                    <option disabled selected>Select Test</option>
                    <?php $result = Test::find_all($_SESSION['course_id']);

                      foreach ($result as $test) { ?>

                        <option value="<?php echo $test->test_id; ?>"><?php echo $test->test_title; ?></option>
                    <?php } ?>
                  </select>

                </div>
                <div class= "form-group col-md-12">
                  <label for="file">Scores</label>
                  <input type="file" name="file" id="file" class="form-control" Placeholder="Scores" required>
                </div>
                <div class="form-group text-center">
                  <input type="submit" name="testdata" value = "Upload Score" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>

        
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Attendance
            </div>
            <div class="panel-body">
              <form method="post" enctype="multipart/form-data">
                <div class="form-group col-md-12">

                  <label>Select Course</label>

                  <select class="form-control" name="att_course" required>
                    <option value="" selected disabled>Select Course</option>
                    <?php $courses = StudentCourse::find_all(); ?>
                    <?php foreach ($courses as $course) { ?>
                      <option value="<?php echo $course->course_id; ?>"><?php echo $course->course_name; ?>(<?php echo $course->course_type; ?>)</option>
                    <?php } ?>
                    
                  </select>

                </div>
                <div class= "form-group col-md-12">
                  <label for="file">Attendance</label>
                  <input type="file" name="file" id="file" class="form-control" Placeholder="Attendance" required>
                </div>
                <div class="form-group text-center">
                  <input type="submit" name="attendance" value = "Upload Attendance" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>
      </div>



      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Assignment
            </div>
            <div class="panel-body">
              <form method="post" >
                <div class="form-group">
                                    <label for="input">Course</label>
                  <select class="form-control" name="sCourse" required>
                    <option value="" selected disabled>Select Course</option>
                    <?php $courses = StudentCourse::find_all(); ?>
                    <?php foreach ($courses as $course) { ?>
                      <option value="<?php echo $course->course_id; ?>"><?php echo $course->course_name; ?>(<?php echo $course->course_type; ?>)</option>
                    <?php } ?>
                    
                  </select>

                </div>
                <div class="form-group">
                                    <label for="input">Assignment Topic</label>
                  <input type="text" name="aTopic" class="form-control" placeholder="Assignment Topic"  required>

                </div>
               <div class="form-group">
                                    <label for="input">Assignment Number</label>
                  <input type="text" name="aNo" class="form-control" placeholder="Assignment no">

                </div>
                <div class="form-group">
                 <label for="input1">Assignment Category</label>
                 <?php if($_SESSION['course_id'] == 14 || $_SESSION['course_id'] == 15){ ?>
               
                  
                  <select name="test_category" id="input1" class="form-control">
                    <option disabled selected>Select Category</option>
                     <option value="Economy">Economy</option>
                    <option value="History">History</option>
                    <option value="Polity">Polity</option> 
                       <option value="Geography">Geography</option> 
                    <option value="CurrentAffairs">Current Affairs</option>                     
                  </select>

                 <?php } else { ?>
              
               
                  <select name="test_category" id="input1" class="form-control">
                    <option disabled selected>Select Category</option>
                    <option value="PHYSICS">Physics</option>
                    <option value="CHEMISTRY">Chemistry</option>
                    <option value="MATHS">Maths</option> 
                    <option value="BIOLOGY">Biology</option>                   
                  </select>

                
                <?php } ?>
                 </div>
                <div class="form-group">
                                     <label for="input">Subject Id</label>
                  <input type="text" name="sId" class="form-control" placeholder="Subject Id"  required>

                </div>
                 <div class="form-group">
                                    <label for="input">Assignment Assigned Date</label>
                  <input type="date" name="assignDate" class="form-control" placeholder="Assign Date"  required>

                </div>
                <div class="form-group">
                                     <label for="input">Assignment Deadline Date</label>
                  <input type="date" name="deadlineDate" class="form-control" placeholder="Deadline Date"  required>

                </div>
                
                <div class="form-group text-center">
                  <input type="submit" name="assignmentinfo" value = "Add Assignment" class="btn btn-danger">
                  
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
              Assignment Topic
            </div>
            <div class="panel-body">
              <form method="post" enctype="multipart/form-data">
                <div class="form-group col-md-12">
                  <label for="input1">Assignment Topic</label>
                  <select name="assignmentid" id="input1" class="form-control" required>
                    <option value="" disabled selected>Select Assignment Topic</option>
                      <?php $result = Assignment::find_all($_SESSION['course_id']);

                      if(!$result) {} else {

                      foreach ($result as $assignment) { ?>

                        <option value="<?php echo $assignment->assignment_id; ?>"><?php echo $assignment->assignment_topic; ?></option>
                      <?php } } ?> 
                  </select>

                </div>
                <div class= "form-group col-md-12">
                  <label for="file">Assignment Details</label>
                  <input type="file" name="file" id="file" class="form-control" Placeholder="AssignmentDetails" required>
                </div>
                <div class="form-group text-center">
                  <input type="submit" name="assignmentdata" value = "Upload Assignment Details" class="btn btn-danger">
                </div>
              </form>
              
            </div>
            
          </div>
      
        </div>
      </div>       



    
      <div class="row">
        <div class="page-header">

          <h4>Drafted Articles</h4>

        </div>
        <div class="panel panel-default">



          <?php 



            $result = Article::get_draft_article();



            if(mysqli_num_rows($result) > 0) {



          ?>



          <ul class="list-group">



            <?php



                while($articles = mysqli_fetch_assoc($result)) {



            ?>



              <li class="list-group-item">

                <a href="article-edit.php?id=<?php echo $articles['article_id']; ?>" class="list-group-item"><?php echo $articles['article_title']; ?></a>

              </li>



            <?php } ?>



          </ul>



          <?php } ?>

        </div>
      </div>     

    </div><!-- /.container -->




     

    <footer class="footer">
      
      <div class="container">

        <h6 class="text-center"> &copy; <?php echo date("Y", time()); ?> CJC Forum | All Rights Reserved | <a href="#">Copyright</a> | <a href="#">Terms & Privacy Policy</a> </h6>

      </div>

    </footer>





    <!-- Bootstrap core JavaScript

    ================================================== -->

    <!-- Placed at the end of the document so the pages load faster -->

    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/tinymce.min.js"></script>

    <script type="text/javascript">
      tinymce.init({
        selector: '.notify',
        height: 300,
        theme: 'modern',
        menubar: false,
        toolbar1: 'undo redo | bold italic'
      });
    </script>



  </body>

</html>