<?php

require_once("includes/initializer.php");
if(!$session->is_logged_in()) {
	redirect_to("".base_url()."/");
}

if($session->logout()) {

	redirect_to("".base_url()."/");
}

?>