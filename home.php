<?php

  require_once("includes/initializer.php");

  if(!$session->is_logged_in()) {

    redirect_to("".base_url()."/login");
  }


  $user = User::find_by_id($session->user_id);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="<?php echo base_url(); ?>/" />
    <link rel="icon" href="images/icon.png">

    <title>Home - CJC Forum</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/reset.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="starter-template.css" rel="stylesheet"> -->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>/"><img alt="CJC Forum" src="images/logo.png"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo base_url(); ?>/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
            <li><a href="articles"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Articles</a></li>
            <li><a href="discussions"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Discussions</a></li>
            <li><a href="test-ocean"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Test Ocean</a></li>
            <li><a href="resources"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> Resources</a></li>
             <li><a href="resources"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> Resources</a></li>
            <?php 
              if($session->is_logged_in()) {
                if($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "finance") {

            ?>
            <li><a href="administrator"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Admin Panel</a></li>
            <?php
                } else {
            ?>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-road" aria-hidden="true"></span> Tracker</a>
                <ul class="dropdown-menu">
                  <li><a href="tracker/attendance/"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Attendance Tracker</a></li>
                  <li><a href="tracker/performance/"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Performance Tracker</a></li>
                   <li><a href="faculty/feedback/"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Faculty Feedback</a></li>
                   <li><a href="schedule/tracker/"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Schedule Tracker</a></li>
                      <li><a href="student/analytics/"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>  Generate Analytics</a></li>
                </ul>
            </li>
             <?php $student = Student::find_all_student($_SESSION['user_id']);  ?>
                     <?php foreach ($student as $result) { ?>
             <li><a href="student/generatepdf/<?php echo $result->student_id;?>"><span class="glyphicon glyphicon-download-alt" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Generate Analytics"></span></a></li>
              <?php } ?>
            <?php      
                }
              }
            ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a class="btn-default dropdown-toggle" id="profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"><span class = "glyphicon glyphicon-user"></span> <small><?php if($session->is_logged_in()) { echo $_SESSION['user_name']; } ?></small></a>
                
              <ul class="dropdown-menu" aria-labelledby="profile">
                <?php 
                  if($session->is_logged_in()) {
                          echo "<li><a href=\"profile\"><span class = \"glyphicon glyphicon-user\"></span> My Profile </a>  </li>";         
                          echo "<li><a href=\"logout\"><span class = \"glyphicon glyphicon-off\"></span> Logout </a>  </li>";     
                        } else {
                          echo "<li> <a href=\"login\"> <span class = \"glyphicon glyphicon-log-in\"></span> Login </a> </li>";
                          echo "<li> <a href=\"signup\"><span class = \"glyphicon glyphicon-edit\"></span> Register </a>  </li>";
                        }
                    ?>
                
              </ul>
            </li>
            
          </ul>
        </div><!--/.nav-collapse -->             
      </div>

    </nav>

    <?php if($user->user_active == 0 && $user->user_type == "admin") { ?>

      <div class="alert alert-danger" role="alert">


        <h5 class="text-center">

          Take a moment to activate your email. 
          <strong>
            <a href="sendlink/<?php echo $session->user_id; ?>/<?php echo slug($_SESSION['user_name']); ?>"> Send Activation</a> 
          </strong>
        </h5>

      </div>

    <?php } ?>

    <?php if(!empty($message)) { ?>

      <div class="alert alert-success" role="alert">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

        <strong>
          <?php echo output_message($message); ?>
        </strong>

      </div>

    <?php } ?>


    <div class="container updates">

      <div class="row">

        <!-- Articles -->

        
        <div class="col-md-3">
          <?php 

            $article_result = Article::get_latest_article();

            if(mysqli_num_rows($article_result) > 0) {


          ?>
            <div class="panel panel-danger">
             <!-- Default panel contents -->
              <div class="panel-heading">

                <h5><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Latest Articles </h5>

              </div>

              <ul class="list-group">
              <?php while($art_row = mysqli_fetch_assoc($article_result)) {

              ?>
                <a href="articles/<?php echo $art_row['article_id']; ?>/<?php echo slug($art_row['article_title']);  ?>/" class="list-group-item">

                  <h4 class="list-group-item-heading"><?php echo $art_row['article_title']; ?></h4>
              <p class="list-group-item-text"><?php echo words_limit($art_row['article_description'],80); ?></p>

                  </a>
                <?php
                  }
                ?>
                </ul>
          
            </div>
          <?php 

            }

          ?>



          <?php 

            $tests_result = TestOcean::get_latest_testocean();

            if(mysqli_num_rows($tests_result) > 0) {


          ?>
          

          <div class="panel panel-danger">
              <!-- Default panel contents -->
              <div class="panel-heading">
                <h5><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Test Ocean </h5>
              
              </div>
              <ul class="list-group">
                <?php while($test_row = mysqli_fetch_assoc($tests_result)) {

                ?>
                  <a href="test-ocean/<?php echo slug($test_row ['main_cat_name']); ?>/<?php echo slug($test_row['sub_cat_name']); ?>" class="list-group-item"> 
                    <h4 class="list-group-item-heading"><?php echo $test_row['main_cat_name']; ?></h4>
                    <p class="list-group-item-text"><?php echo words_limit($test_row['question'],80); ?></p>
                  </a>
                <?php
                  }
                ?>
              </ul>
          
          </div>
            <?php 

            }

          ?>

        </div>

        <div class="col-md-6">

          <?php

            $result = Post::get_follow_discussion($_SESSION['user_id']);

            if(mysqli_num_rows($result) > 0) {

          ?>

                      
              <h3 class="text-center">Posts Thats You Follow</h3>


          <?php

              while($post = mysqli_fetch_assoc($result)) {

              //$dir = $upload_dir.$post['post_image']; 
          ?>

                                       

                <div class="panel panel-default">
                
                  <div class="list-group-item">                   

                    <form method="post" id="followme<?php echo $post['post_id']; ?>"  >
                          <input type="hidden" name="postId" value="<?php echo $post['post_id']; ?>">
                          <input type="hidden" name="userId" value="<?php echo $_SESSION['user_id']; ?>">
                      </form>
                      <?php 
                        $follow = new Follow();

                        $follow->post_id = $post['post_id'];
                        $follow->user_id = $_SESSION['user_id'];

                        if($follow->find()) {

                      ?>

                          <div id="follow-<?php echo $post['post_id']; ?>" class="pull-right"> <button onClick="myFunction(<?php echo $post['post_id']; ?>,'unfollow',<?php echo $post['post_followers']; ?>)"  name="follow" class="btn btn-success">Following <span class="badge"><?php echo $post['post_followers']; ?></span></button></div>

                      <?php

                        } else {

                      ?>

                          <div id="follow-<?php echo $post['post_id']; ?>" class="pull-right"> <button onClick="myFunction(<?php echo $post['post_id']; ?>,'follow',<?php echo $post['post_followers']; ?>)"  name="follow" class="btn btn-danger">Follow <span class="badge"><?php echo $post['post_followers']; ?></span></button></div>

                     
                      <?php

                        }

                      ?> 

                                       
                      <h4><a  href="discussions/<?php echo $post['post_id']; ?>/<?php echo slug($post['post_title']); ?>"> <?php echo $post['post_title'] ?> </a> <small><span class="label label-danger"><?php echo $post['post_category']; ?></span></small></h4> 
                      <p> <?php echo words_limit($post['post_description'],130); ?> </p>
                      <small><?php echo done_by($post['user_name'], $post['post_time']); ?></small>
                    
                    </div>
                </div>
                                      
              

          <?php

            }
          } else {
          ?>

              <div class="well">
              
                <h5>Looks Like You are new here.. </h5>
                <p> Posts that you follow will be displayed here... </p>

              </div>

          <?php

                }
             ?>
                
          <div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="text-center"> Please <strong>Login</strong> or <strong>Signup</strong></h4>
                  <h4 class="text-center"><a class="btn btn-info" href="login.php"> Go CJC Forum </a></h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        


        <!-- Posts -->
        
        <div class="col-md-3">

          <?php 

            $posts_result = Post::get_latest_discussion();

            if(mysqli_num_rows($posts_result) > 0) {


          ?>
          <div class="panel panel-danger">
            <!-- Default panel contents -->
              <div class="panel-heading">

                <h5><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Discussions </h5>

              </div>

              <ul class="list-group">
                <?php while($post_row = mysqli_fetch_assoc($posts_result)) {

                ?>
                  <a href="discussions/<?php echo $post_row['post_id']; ?>/<?php echo slug($post_row['post_title']); ?>" class="list-group-item">

                    <h4 class="list-group-item-heading"><?php echo $post_row['post_title']; ?></h4>
              <p class="list-group-item-text"><?php echo words_limit($post_row['post_description'],80); ?></p>
                  </a>
                <?php
                  }
                ?>
              </ul>
          
            </div>
            <?php 

            }

          ?>

          <?php 

            $res_result = Resource::get_latest_resource();

            if(mysqli_num_rows($res_result) > 0) {


          ?>          

          <div class="panel panel-danger">
              <!-- Default panel contents -->
              <div class="panel-heading">
                <h5><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> Popular Resources </h5>
              </div>
              <ul class="list-group">
                <?php while($res_row = mysqli_fetch_assoc($res_result)) {

                ?>
                  <a href="resources/<?php echo $res_row['resource_id']; ?>/<?php echo slug($res_row['resource_title']); ?>" class="list-group-item">
                    <h4 class="list-group-item-heading"><?php echo $res_row['resource_category']; ?></h4>
                    <p class="list-group-item-text"><?php echo words_limit($res_row['resource_title'],80); ?></p>
                  </a>
                <?php
                  }
                ?>
              </ul>
            
          </div>

          <?php 

            }

          ?>

        </div>
        
   
      </div>


    </div><!-- /.container -->

   


    <footer class="footer">
      <div class="container">
        <h5 class="text-center"> &copy; <?php echo date("Y", time()); ?> CJC Forum | All Rights Reserved | <a href="#">Copyright</a> | <a href="#">Terms & Privacy Policy</a> </h5>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function myFunction(id,action,follows) {

        var form = $('#followme'+id);
        
        $.ajax({
          url: "newfollowing.php",
          data: form.serialize(),
          type: "POST",
          beforeSend: function() {
            $('#follow-'+id).html("<img src='images/loaderIcon.gif' />");
          },
          success: function(data){
            switch(action) {
              case "follow":
              follows++;
              $('#follow-'+id).html('<button onClick="myFunction('+id+',\'unfollow\','+follows+')"  name="follow" class="btn btn-success">Following <span class="badge">'+ follows +'</span></button>');
              
              break;
              case "unfollow":
              follows--;
              $('#follow-'+id).html('<button onClick="myFunction('+id+',\'follow\','+follows+')"  name="follow" class="btn btn-danger">Follow <span class="badge">'+ follows +'</span></button>')
              
              break;
            }
          
          }
        });
    }
    </script>
  </body>
</html>