<?php



	require_once("includes/initializer.php");



	$username="";

	$password="";





	if($session->is_logged_in()) {



		redirect_to("".base_url()."/");





	}



	if(isset( $_POST['submit'])) {



		$username = trim($_POST['f_username']);

		$password = trim($_POST['f_password']);



		$found_user = User::authenticate($username, $password);



		if($found_user) {



			$session->login($found_user);



			redirect_to("".base_url()."/");



		} else {



			$session->message("Username/password incorrect");

			redirect_to("".base_url()."/login");

		}

	} 





?>



<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<meta name="description" content="">

		<meta name="author" content="">

		<base href="<?php echo base_url(); ?>/" />

    	<link rel="icon" href="images/icon.png">



		<title>CJC Forum - Join</title>



		<!-- Bootstrap core CSS -->

		<link href="css/bootstrap.css" rel="stylesheet">

		<link href="css/reset.css" rel="stylesheet">



		<!-- Custom styles for this template -->

		<!-- <link href="starter-template.css" rel="stylesheet"> -->



		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->

		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

		<!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->



		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>

		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

		<![endif]-->

	</head>



  <body>



    <nav class="navbar navbar-inverse navbar-fixed-top">

      <div class="container">

        <div class="navbar-header">



          <a class="navbar-brand" href="<?php echo base_url(); ?>/"><img alt="CJC Forum" src="images/logo.png"></a>

        </div>

      </div>



    </nav>



    <div class="container updates">



      <div class="row">



      	<div class="col-md-12">



      		<?php if(!empty($message)) { ?>



		      <div class="alert alert-success" role="alert">



		        <button type="button" class="close" data-dismiss="alert" aria-label="Close">

		          <span aria-hidden="true">&times;</span>

		        </button>



		        <strong>

		          <?php echo output_message($message); ?>

		        </strong>



		      </div>



		    <?php } ?>



			<div class="col-md-4 col-md-offset-4 panel panel-default">



				<div class="panel-body">

					<div class="col-md-12 text-center">

						<h4>Login Into <strong>CJC Forum</strong></h4>	

						<hr>				

					</div>

					<div class="col-md-12">

						<form method="post">

							<div class="form-group">

								<input type="email" class="form-control"  tabindex="1" name ="f_username" placeholder="Enter Email" value = "<?php echo htmlentities($username); ?>" required>

							</div>

							<div class="form-group">

								<input type="password" class="form-control" tabindex="2" name = "f_password" minlength="8" maxlength="25" placeholder="Enter Password" value = "<?php echo htmlentities($password); ?>" required>

							</div>

							<div class="form-group">

								<div class="row">

								  <div class="col-sm-6 col-sm-offset-3">

								    <input type="submit" name="submit" tabindex="3" class="form-control btn btn-success" value="Log In">

								  </div>

								</div>

							</div>

							<div class="form-group">

								<div class="row">

								  <div class="col-md-12">

								    <div class="text-center">

								      <a href="forgotpassword" tabindex="5" class="forgot-password">Forgot Password?</a>

								    </div>

								  </div>

								</div>

							</div>


				        </form>						  

						<hr>

					</div>					

				</div>



	        </div>      					

      		

      	</div>

        

      </div>

    </div>





    <footer class="footer">

      <div class="container">

        <h5 class="text-center"> &copy; <?php echo date("Y", time()); ?> CJC Forum | All Rights Reserved | <a href="#">Copyright</a> | <a href="#">Terms & Privacy Policy</a> </h5>

      </div>

    </footer>





    <!-- Bootstrap core JavaScript

    ================================================== -->

    <!-- Placed at the end of the document so the pages load faster -->



    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/bootstrap.min.js"></script>



  </body>

</html>